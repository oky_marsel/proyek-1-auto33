<!DOCTYPE html>
<html lang="en">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AUTO 33</title>

    <!-- favicon
		============================================ -->
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">

    <!-- Bootstrap -->
    <link href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    
    <!-- Custom styling plus plugins -->
    <link href="{{ asset('build/css/custom.min.css')}}" rel="stylesheet">
  </head>

  <body class="">
    @include('pengunjung.navbar')
    <div class="container">
        <!-- page content home -->
        <div class="container" id="home" style="padding-top:70px;">
          <div class="">
            <div class="image view view-first">
              <img src="img/mobil.jpg" style="width: 100%; display: block; margin: auto;" alt="image">
              <div class="mask" style="padding-top:70px; padding-bottom:100px ">
                <img src="img/logo/logo1.png" alt="" style="margin:auto;"/>
                <!-- <h1 style="color:white;"><b>SHOWROOM AUTO 33</b></h1> -->
                  <h6 style="color:white; padding:0px 100px 0px 100px;">Toko kami menyediakan berbagai macam mobil bekas berkualitas. toko kami sangat strategis dengan harga yang terjangkau harga ekonomis <br>toko kami memiliki keunggulan yaitu tempat strategis di pinggir jalan banyak diliat orang dan kualitas produk sangat bagus, mari beli di toko kami <br>kami siap melayani anda terima kasih.</h6>
                </div>
            </div>
          </div>
        </div>
        <!-- /page content home -->

        <br>

        <!-- page content katalog -->
 <div class="x_panel" role="main" id="katalog">   
              <div class="col-md-12">
            <div class="row">
                  <div class="x_panel" >
                    <div class="x_title">
                      <h1 class="text-center"><b>Katalog Mobil</b></h1>
                      <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                      <div class="row">
                      @foreach($transaksi as $b)
                        <div class="col-md-55">
                          <div class="thumbnail">
                            <div class="image view view-first">
                              <img style="width: 100%; display: block;" src="{{url('/data_file/'.$b->gbr_mobil)}}" alt="image" />
                              <div class="mask">
                                <div class="tools tools-bottom">
                                  <a href="/detil/{{$b->id}}"><p>klik untuk melihat detail</p></a>
                                </div>
                              </div>
                            </div>
                            <div class="caption">
                                <p><strong>IDR.{{number_format($b->hrg_penjualan)}}</strong></p>
                              <p>{{$b->tipe_mobil}} ({{$b->plat_mobil}})</p> 
                            </div>
                          </div>
                        </div>
                        @endforeach
                      </div>
                    </div>

                    </div> 
                  </div>
              </div>
            
          </div>
          <!-- /page content katalog -->
          

       <br>
       <br>
          <!-- contact -->
    <div class="x_panel" id="contact">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1><b>Contact</b></h1>
                <hr>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-4">
                    <div class="p-3 bg-info text-center text-white">
                    <h5>Contact Me</h5>
                    Silahkan kunjungi Alamat Showroom Auto 33
                    </div>
                    <ul class="list-group mt-3">
                        <li class="list-group-item"><h4>Lokasi</h4></li>
                        <li class="list-group-item"><b>Pusat </b>: Tosaren, Kediri
                          <br>086567765444
                        </li>
                        <li class="list-group-item"><b>Cabang </b>: Gampengrejo Kediri
                          <br>081234432123
                        </li>
                    </ul>
                </div>
            <div class="col-lg-6">
                <div class="card card-body rounded-0">
                    <form action="">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control">
                        </div>
                        <div class="form-group">
                          <label>Email</label>
                          <input type="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="phone" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Pesan Anda</label>
                            <textarea class="form-control"></textarea>
                        </div>
                        <input type="submit" class="btn btn-primary" value="Send">
                        <input type="reset" class="btn btn-danger" value="Reset">
                    </form>
                </div>
            </div>
        </div>
    </div>
<!-- End contact -->

    </div>

    <!-- jQuery -->
    <script src="{{ asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
   <script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{ asset('vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{ asset('vendors/nprogress/nprogress.js')}}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{ asset('build/js/custom.min.js')}}"></script>
  </body>
</html>