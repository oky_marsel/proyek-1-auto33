<!DOCTYPE html>
<html lang="en">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AUTO 33</title>

    <!-- favicon
		============================================ -->
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">

    <!-- Bootstrap -->
    <link href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('vendors/nprogress/nprogress.css')}}" rel="stylesheet">
    
    <!-- Custom styling plus plugins -->
    <link href="{{ asset('build/css/custom.min.css')}}" rel="stylesheet">
  </head>

  <body class="">
    @include('pengunjung.navbar')
    <div class="container">

        <!-- page content katalog -->

        <div class="x_panel" style="padding-top:100px;" method="POST">
        
        @method('PUT')  
                                                {{csrf_field()}}
              <div class="row">
                  <div class="col-md-12 col-sm-12 ">
                    <div class="x_panel">
                      <div class="x_title">
                        <h1 class="text-center"><b>{{$transaksi->tipe_mobil}}</b></h1>
                        
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
    
                        <div class="col-md-5 col-sm-5 ">
                          <div class="product-image">
                            <img src="{{url('/data_file/'.$transaksi->gbr_mobil)}}" alt="..." />
                          </div>
                          <!-- <div class="product_gallery" >
                            <a>
                              <img src="{{url('/data_file/'.$transaksi->gbr_mobil)}}"  alt="..." />
                            </a>
                            <a>
                              <img src="{{url('/data_file/'.$transaksi->gbr_mobil)}}" alt="..." />
                            </a>
                            <a>
                              <img src="{{url('/data_file/'.$transaksi->gbr_mobil)}}" alt="..." />
                            </a>
                            <a>
                              <img src="{{url('/data_file/'.$transaksi->gbr_mobil)}}" alt="..." />
                            </a>
                          </div> -->
                        </div>
    
                        <div class="col-md-7 col-sm-7 " style="border:0px solid #e5e5e5;">
    
                          <h3 class="prod_title">{{$transaksi->plat_mobil}} - {{$transaksi->mesin_mobil}}</h3>
    
                          <p>{{$transaksi->ket_mobil}}</p>
                          <br >
    
                          <div class="">
                            <div class="product_price">
                              <h1 class="price">IDR.{{number_format($transaksi->hrg_penjualan)}}</h1>
                              <!-- <span class="price-tax">nego tipis tipis</span>
                              <br> -->
                            </div>
                          </div>
    
                          <div class="">
                          </div>
    
                          <div class="product_social">
                              <button type="button" class="btn btn-default btn-lg">Hubungi WA/Telp.</button>
                            <ul class="list-inline display-layout">
                              <li><a href="#"><i class="fa fa-whatsapp"> 086789987666</i></a>
                              </li>
                            </ul>
                          </div>
    
                        </div>
    
    
                        
                      </div>
                    </div>
                  </div>
                  
                </div>
          </div>
          <!-- /page content katalog -->

    </div>

    <!-- jQuery -->
    <script src="{{ asset('vendors/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
   <script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{ asset('vendors/fastclick/lib/fastclick.js')}}"></script>
    <!-- NProgress -->
    <script src="{{ asset('vendors/nprogress/nprogress.js')}}"></script>

    <!-- Custom Theme Scripts -->
    <script src="{{ asset('build/js/custom.min.js')}}"></script>
  </body>
</html>