<!-- awal navbar -->
<nav class="navbar navbar-expand-md navbar-light fixed-top shadow" style="background-color: #2A3F54;" >
        <div class="container">
          <a class="navbar-brand" href="/">
            <h3><b>Auto 33</b></h3> 
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                      <a class="nav-link" href="/#home">Home</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="/#katalog">Katalog</a>
                  </li>
                  <li class="nav-item">
                          <a class="nav-link" href="/#contact">Contact</a>
                  </li>
              </ul>
                <div class="title_left">
                  <div class=" form-group top_search">
        
                      <span class="input-group-btn">
                        <a href="{{ route('login') }}" >
                          <button class="btn btn-light" type="button">LogIn</button>
                        </a>
                      </span>
                    
                  </div>
                </div>
          </div>
        </div>
      </nav>
      <!-- akhir navbar -->