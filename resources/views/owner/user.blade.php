@extends('owner.master')

@section('content')
<div class="all-content-wrapper">
    <div class="breadcome-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcome-list single-page-breadcome">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="breadcome-heading">
                                    <form role="search" class="sr-input-func">
                                        <input type="text" placeholder="Search..." class="search-int form-control">
                                        <a href="#"><i class="fa fa-search"></i></a>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <ul class="breadcome-menu">
                                    <li><a href="/dashboard">Home</a> <span class="bread-slash">/</span>
                                    </li>
                                    <li><span class="bread-blod">Data User</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product-status mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="product-status-wrap">
                        <h4>Data User</h4>
                        @if(session('sukses'))
                        <div class="alert alert-success alert-success-style1 alert-st-bg alert-st-bg11">
                        <i class="fa fa-check edu-checked-pro admin-check-pro admin-check-pro-clr admin-check-pro-clr11" aria-hidden="true"></i>
                        {{session('sukses')}}
                                <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
										<span class="icon-sc-cl" aria-hidden="true">&times;</span>
									</button>                                
                            </div>
                         @endif
                        <div class="add-product">
                            <a href="/createuser">Tambahkan User</a>
                        </div>
                        <div class="asset-inner">
                            <table class="table table-bordered table-hover">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col" class="centered">No</th>
                                    <th scope="col" class="centered">NIK</th>
                                    <th scope="col" class="centered">Nama</th>
                                    <th scope="col" class="centered">TTL</th>
                                    <th scope="col" class="centered">Jenis Kelamin</th>
                                    <th scope="col" class="centered">No.Hp</th>
                                    <th scope="col" class="centered">Alamat</th>
                                    <th scope="col" class="centered">Username</th>
                                    <th scope="col" class="centered">Email</th>
                                    <th scope="col" class="centered">Level</th>
                                    <th cscope="col" lass="centered">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                <?php $no=1; ?>
                                    @foreach($pengguna as $b)
                                    <td><?php  echo $no;$no++;?></td>
                                    <td>{{$b->nik}}</td>
                                    <td>{{$b->nama}}</td>
                                    <td>{{$b->tmp_lhr}}, {{$b->lahir_usr}}</td>
                                    <td>{{$b->jk_usr}}</td>
                                    <td>{{$b->hp_usr}}</td>
                                    <td>{{$b->alamat}}</td>
                                    <td>{{$b->username}}</td>
                                    <td>{{$b->email}}</td>
                                    <td>{{$b->role}}</td>
                                    <td>
                                        <a href="/edituser/{{$b->id}}" data-toggle="tooltip" title="Edit" class="pd-setting-ed"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        <a href="/desuser:{{$b->id}}" data-toggle="tooltip" title="Hapus" class="pd-setting-ed"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                                @endforeach
                            </table>
                        </div>
                        <div class="custom-pagination">
                            <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection