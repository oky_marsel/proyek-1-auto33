@extends('owner.master')

@section('content')
<div class="all-content-wrapper">
    <div class="breadcome-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcome-list single-page-breadcome">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="breadcome-heading">
                                    <form role="search" class="sr-input-func">
                                        <input type="text" placeholder="Search..." class="search-int form-control">
                                        <a href="#"><i class="fa fa-search"></i></a>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <ul class="breadcome-menu">
                                    <li><a href="/dashboard">Home</a> <span class="bread-slash">/</span>
                                    </li>
                                    <li><span class="bread-blod">Tambah Transaksi</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Single pro tab review Start-->
    <div class="single-pro-review-area mt-t-30 mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="product-payment-inner-st">
                        <ul id="myTabedu1" class="tab-review-design">
                            <li class="active"><a href="#description">Tambahkan Transaksi</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content custom-product-edit">
                            <div class="product-tab-list tab-pane fade active in" id="description">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <div id="dropzone1" class="pro-ad addcoursepro">
                                                <form action="/storetrx" method="POST" enctype="multipart/form-data">
                                                    {{csrf_field()}}
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>ID Transaksi</label>
                                                                <input type="text" name="id_trx" id="id_trx" class="form-control" value="{{old('id_trx')}}" placeholder="trxxxx">
                                                                @if($errors->has('id_trx'))
                                                                <div class="text-danger">
                                                                    {{$errors->first('id_trx')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label>No. Mesin</label>
                                                                <input type="text" name="mesin_mobil" id="mesin_mobil" class="form-control" value="{{old('mesin_mobil')}}" placeholder="masukkan nomor mesin mobil">
                                                                @if($errors->has('mesin_mobil'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('mesin_mobil')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Plat Nomor</label>
                                                                <input type="text" name="plat_mobil" id="plat_mobil" class="form-control" value="{{old('plat_mobil')}}" placeholder="ex : AG 0809 BB">
                                                                @if($errors->has('plat_mobil'))
                                                                <div class="text-danger">
                                                                    {{$errors->first('plat_mobil')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Nama Mobil</label>
                                                                <input type="text" name="tipe_mobil" id="tipe_mobil" class="form-control" value="{{old('tipe_mobil')}}" placeholder="ex : Honda Brio">
                                                                @if($errors->has('tipe_mobil'))
                                                                <div class="text-danger">
                                                                    {{$errors->first('tipe_mobil')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Tahun Rilis</label>
                                                                <input type="text" name="tahun_mobil" id="tahun_mobil" class="form-control" value="{{old('tahun_mobil')}}" placeholder="ex : 2011">
                                                                @if($errors->has('tahun_mobil'))
                                                                <div class="text-danger">
                                                                    {{$errors->first('tahun_mobil')}}
                                                                </div>
                                                                @endif
                                                            </div>                                                  
                                                            <div class="form-group mx-sm-3 mb-2">
                                                                <label>Foto Mobil</label>                                                              
                                                                    <!-- <a class="btn" title="Insert picture (or just drag &amp; drop)" id="pictureBtn">
                                                                        <i class="fa fa-picture-o"></i>
                                                                    </a> -->
                                                                <input type="file" class="form-control-file" name="gbr_mobil" id="gbr_mobil" type="file" >   
                                                                @if($errors->has('gbr_mobil'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('gbr_mobil')}}
                                                                </div>
                                                                @endif                                                             
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Nama Penjual</label>
                                                                <input type="text" name="id_penjual" id="id_penjual" class="form-control" value="{{old('id_penjual')}}" placeholder="nama penjual mobil">
                                                                @if($errors->has('id_penjual'))
                                                                <div class="text-danger">
                                                                    {{$errors->first('id_penjual')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group col-lg-6 col-md-6">
                                                                <label>Harga Beli</label>
                                                                <input name="hrg_pembelian" id="hrg_pembelian" type="text" class="form-control" value="{{ old('hrg_pembelian') }}" placeholder="ex : 12000000">
                                                                @if($errors->has('hrg_pembelian'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('hrg_pembelian')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group col-lg-6 col-md-6" id="tgl_pembelian">
                                                                <label>Tanggal Beli</label>
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                    <input type="date" name="tgl_pembelian" id="tgl_pembelian" class="form-control" value="{{ old('tgl_pembelian') }}">
                                                                    @if($errors->has('tgl_pembelian'))
                                                                        <div class="text-danger">
                                                                            {{ $errors->first('tgl_pembelian')}}
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="form-group">
                                                                <label>deskrisi Mobil</label>
                                                                <textarea name="ket_mobil" id="ket_mobil" value="{{old('ket_mobil')}}"  placeholder="keterangan mobil yang ingin dijual"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="payment-adress">
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection