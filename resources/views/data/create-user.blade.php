@extends('owner.master')

@section('content')
<div class="all-content-wrapper">
    <div class="breadcome-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcome-list single-page-breadcome">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="breadcome-heading">
                                    <form role="search" class="sr-input-func">
                                        <input type="text" placeholder="Search..." class="search-int form-control">
                                        <a href="#"><i class="fa fa-search"></i></a>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <ul class="breadcome-menu">
                                    <li><a href="#">Home</a> <span class="bread-slash">/</span>
                                    </li>
                                    <li><span class="bread-blod">Tambah User</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Single pro tab review Start-->
    <div class="single-pro-review-area mt-t-30 mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="product-payment-inner-st">
                        <ul id="myTabedu1" class="tab-review-design">
                            <li class="active"><a href="#description">Tambahkan User</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content custom-product-edit">
                            <div class="product-tab-list tab-pane fade active in" id="description">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <div id="dropzone1" class="pro-ad addcoursepro">
                                                <form action="/storeuser" method="POST" enctype="multipart/form-data">
                                                    {{csrf_field()}}
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>NIK</label>
                                                                <input name="nik" id="nik" type="text" class="form-control" value="{{ old('nik') }}" placeholder="35xxxxxxxxxxxxxx">
                                                                @if($errors->has('nik'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('nik')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Nama</label>
                                                                <input name="nama" id="nama" type="text" class="form-control" value="{{ old('nama') }}" placeholder="Nama Lengkap">
                                                                @if($errors->has('nama'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('nama')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Jenis Kelamin</label>
                                                                    <select name="jk_usr" class="form-control">
																		<option value="none" selected="" disabled="">Pilih Jenis Kelamin</option>
																		<option value="Laki-laki">Laki-laki</option>
																		<option value="Perempuan">Perempuan</option>
                                                                    </select>
                                                                    @if($errors->has('jk_usr'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('jk_usr')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Username</label>
                                                                <input name="username" id="username" type="text" class="form-control" value="{{ old('username') }}" placeholder="Username">
                                                                @if($errors->has('username'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('username')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Email</label>
                                                                <input name="email" id="email" type="email" class="form-control" value="{{ old('email') }}" placeholder="email">
                                                                @if($errors->has('email'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('email')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Level</label>
                                                                    <select name="role" class="form-control">
																		<option value="none" selected="" disabled="">Pilih Level</option>
																		<option value="owner">Owner</option>
																		<option value="pegawai">pegawai</option>
                                                                    </select>
                                                                    @if($errors->has('role'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('role')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <div class="form-group col-lg-6 col-md-6">
                                                                <label>Tempat</label>
                                                                <input name="tmp_lhr" id="tmp_lhr" type="text" class="form-control" value="{{ old('tmp_lhr') }}" placeholder="Tempat Lahir">
                                                                @if($errors->has('tmp_lhr'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('tmp_lhr')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group data-custon-pick col-md-6" id="lahir_usr">
                                                                <label>Tanggal Lahir</label>
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                    <input type="date" name="lahir_usr" id="lahir_usr" class="form-control" value="{{ old('lahir_usr') }}">
                                                                    @if($errors->has('lahir_usr'))
                                                                        <div class="text-danger">
                                                                            {{ $errors->first('lahir_usr')}}
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>No. Handphone</label>
                                                                <input name="hp_usr" id="hp_usr" type="text" class="form-control" value="{{ old('hp_usr') }}" placeholder="+628xxxxxxxxxx">
                                                                @if($errors->has('hp_usr'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('hp_usr')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group res-mg-t-15">
                                                                <label>Alamat</label>
                                                                    <textarea name="alamat" id="alamat" value="{{old('alamat')}}" placeholder="Alamat"></textarea>
                                                                </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="payment-adress">
                                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection