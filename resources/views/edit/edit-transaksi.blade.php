@extends('owner.master')

@section('content')
<div class="all-content-wrapper">
    <div class="breadcome-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcome-list single-page-breadcome">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="breadcome-heading">
                                    <form role="search" class="sr-input-func">
                                        <input type="text" placeholder="Search..." class="search-int form-control">
                                        <a href="#"><i class="fa fa-search"></i></a>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <ul class="breadcome-menu">
                                    <li><a href="/dashboard">Home</a> <span class="bread-slash">/</span>
                                    </li>
                                    <li><a href="/transaksi">Transaksi</a> <span class="bread-slash">/</span>
                                    </li>
                                    <li><span class="bread-blod">Edit Transaksi</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Single pro tab review Start-->
    <div class="single-pro-review-area mt-t-30 mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="product-payment-inner-st">
                        <ul id="myTabedu1" class="tab-review-design">
                            <li class="active"><a href="#description">Edit Katalog</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content custom-product-edit">
                            <div class="product-tab-list tab-pane fade active in" id="description">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <div id="dropzone1" class="pro-ad addcoursepro">
                                                <form action="/updtrx/{{$transaksi->id}}" method="POST" enctype="multipart/form-data">
                                                @method('PUT')  
                                                {{csrf_field()}}
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                                <label>ID Transaksi</label>
                                                                <input type="text" name="id_trx" id="id_trx" class="form-control" value="{{$transaksi->id_trx}}" readonly>
                                                                @if($errors->has('id_trx'))
                                                                <div class="text-danger">
                                                                    {{$errors->first('id_trx')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label>No Mesin</label>
                                                                <input name="mesin_mobil" id="mesin_mobil" type="text" class="form-control" value="{{$transaksi->mesin_mobil}}" readonly>
                                                                @if($errors->has('mesin_mobil'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('mesin_mobil')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Plat Nomor</label>
                                                                <input name="plat_mobil" id="plat_mobil" type="text" class="form-control" value="{{$transaksi->plat_mobil}}" readonly>
                                                                @if($errors->has('plat_mobil'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('plat_mobil')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Nama Mobil</label>
                                                                <input name="tipe_mobil" id="tipe_mobil" type="text" class="form-control" value="{{$transaksi->tipe_mobil}}" readonly>
                                                                @if($errors->has('tipe_mobil'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('tipe_mobil')}}
                                                                </div>
                                                                @endif
                                                            </div>            
                                                            
                                                            <div class="form-group">
                                                            <label>Tahun Rilis</label>
                                                                <input name="tahun_mobil" id="tahun_mobil" type="year" class="form-control" value="{{$transaksi->tahun_mobil}}" readonly>
                                                                @if($errors->has('tahun_mobil'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('tahun_mobil')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Keterangan Mobil</label>
                                                                    <input name="ket_mobil" id="ket_mobil" value="{{$transaksi->ket_mobil}}" class="form-control" readonly>
                                                            </div>  
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">    
                                                            <div class="form-group">
                                                                <label>Nama Penjual</label>
                                                                <input type="text" name="id_penjual" id="id_penjual" class="form-control" value="{{$transaksi->id_penjual}}" readonly>
                                                                @if($errors->has('id_penjual'))
                                                                <div class="text-danger">
                                                                    {{$errors->first('id_penjual')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group col-lg-6 col-md-6">
                                                                <label>Harga Beli</label>
                                                                <input name="hrg_pembelian" id="hrg_pembelian" type="text" class="form-control" value="{{$transaksi->hrg_pembelian}}" readonly>
                                                                @if($errors->has('hrg_pembelian'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('hrg_pembelian')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group col-lg-6 col-md-6" id="tgl_pembelian">
                                                                <label>Tanggal Beli</label>
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                    <input type="date" name="tgl_pembelian" id="tgl_pembelian" class="form-control" value="{{$transaksi->tgl_pembelian}}" readonly>
                                                                    @if($errors->has('tgl_pembelian'))
                                                                        <div class="text-danger">
                                                                            {{ $errors->first('tgl_pembelian')}}
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Nama Pembeli</label>
                                                                <input type="text" name="id_pembeli" id="id_pembeli" class="form-control" value="{{$transaksi->id_pembeli}}" placeholder="Masukkan Nama Pembeli">
                                                                @if($errors->has('id_pembeli'))
                                                                <div class="text-danger">
                                                                    {{$errors->first('id_pembeli')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label>Harga Jual</label>
                                                                <input name="hrg_penjualan" id="hrg_penjualan" type="text" class="form-control" value="{{$transaksi->hrg_penjualan}}" readonly>
                                                                @if($errors->has('hrg_penjualan'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('hrg_penjualan')}}
                                                                </div>
                                                                @endif
                                                                </div>
                                                            <div class="form-group col-lg-6 col-md-6" id="tgl_penjualan">
                                                                <label>Tanggal Jual</label>
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                                    <input type="date" name="tgl_penjualan" id="tgl_penjualan" class="form-control" value="{{$transaksi->tgl_penjualan}}">
                                                                    @if($errors->has('tgl_penjualan'))
                                                                        <div class="text-danger">
                                                                            {{ $errors->first('tgl_penjualan')}}
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>                                                                                                                        
                                                        </div>
                                                                                                              
                                                    </div>

                                                    <div class="row">
                                                    <div class="col-lg-12">
                                                            <div class="payment-adress">
                                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection