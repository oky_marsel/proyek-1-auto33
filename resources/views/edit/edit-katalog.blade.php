@extends('owner.master')

@section('content')
<div class="all-content-wrapper">
    <div class="breadcome-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcome-list single-page-breadcome">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="breadcome-heading">
                                    <form role="search" class="sr-input-func">
                                        <input type="text" placeholder="Search..." class="search-int form-control">
                                        <a href="#"><i class="fa fa-search"></i></a>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <ul class="breadcome-menu">
                                    <li><a href="/dashboard">Home</a> <span class="bread-slash">/</span>
                                    </li>
                                    <li><a href="/katalog">Katalog</a> <span class="bread-slash">/</span>
                                    </li>
                                    <li><span class="bread-blod">Edit Katalog</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Single pro tab review Start-->
    <div class="single-pro-review-area mt-t-30 mg-b-15">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="product-payment-inner-st">
                        <ul id="myTabedu1" class="tab-review-design">
                            <li class="active"><a href="#description">Edit Katalog</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content custom-product-edit">
                            <div class="product-tab-list tab-pane fade active in" id="description">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="review-content-section">
                                            <div id="dropzone1" class="pro-ad addcoursepro">
                                                <form action="/updktlg/{{$transaksi->id}}" method="POST" enctype="multipart/form-data">
                                                @method('PUT')  
                                                {{csrf_field()}}
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>No Mesin</label>
                                                                <input name="mesin_mobil" id="mesin_mobil" type="text" class="form-control" value="{{$transaksi->mesin_mobil}}" readonly>
                                                                @if($errors->has('mesin_mobil'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('mesin_mobil')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Plat Nomor</label>
                                                                <input name="plat_mobil" id="plat_mobil" type="text" class="form-control" value="{{$transaksi->plat_mobil}}">
                                                                @if($errors->has('plat_mobil'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('plat_mobil')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Nama Mobil</label>
                                                                <input name="tipe_mobil" id="tipe_mobil" type="text" class="form-control" value="{{$transaksi->tipe_mobil}}">
                                                                @if($errors->has('tipe_mobil'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('tipe_mobil')}}
                                                                </div>
                                                                @endif
                                                            </div>            
                                                            
                                                            <div class="form-group">
                                                            <label>Tahun Rilis</label>
                                                                <input name="tahun_mobil" id="tahun_mobil" type="year" class="form-control" value="{{$transaksi->tahun_mobil}}">
                                                                @if($errors->has('tahun_mobil'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('tahun_mobil')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> 
                                                        <div class="form-group col-md-6">                                                
                                                            <label>Harga Beli</label>
                                                                <input name="hrg_pembelian" id="hrg_pembelian" type="text" class="form-control" value="{{$transaksi->hrg_pembelian}}" readonly>
                                                                @if($errors->has('hrg_pembelian'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('hrg_pembelian')}}
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label>Harga Jual</label>
                                                                <input name="hrg_penjualan" id="hrg_penjualan" type="text" class="form-control" value="{{$transaksi->hrg_penjualan}}">
                                                                @if($errors->has('hrg_penjualan'))
                                                                <div class="text-danger">
                                                                    {{ $errors->first('hrg_penjualan')}}
                                                                </div>
                                                                @endif
                                                                </div>
                                                                                                                     
                                                            <div class="form-group">
                                                                <label>Keterangan Mobil</label>
                                                                    <input name="ket_mobil" id="ket_mobil" value="{{$transaksi->ket_mobil}}" class="form-control">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Foto Mobil</label>
                                                                <br>
                                                                <div class="btn-group">
                                                                    <div style="padding-bottom: 10px">
                                                                        <img src='../data_file/{{$transaksi->gbr_mobil}}' width='100' height='100' />   
                                                                    </div>
                                                                    <input name="gbr_mobil" id="gbr_mobil" type="file" value="{{$transaksi->gbr_mobil}}" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="row">
                                                    <div class="col-lg-12">
                                                            <div class="payment-adress">
                                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection