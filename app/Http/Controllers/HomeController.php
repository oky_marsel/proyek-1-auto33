<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $transaksi = transaksi::where('hrg_penjualan', 0)->get();
        $transaksi = transaksi::where('hrg_penjualan','<>','')->get();
        //$transaksi = transaksi::all();
        return view('pengunjung.home', ['transaksi' => $transaksi]);
    }

    public function show($id)
    {
        $transaksi = transaksi::find($id);
        return view('pengunjung.detil-katalog', ['transaksi' => $transaksi]);
    }
}
