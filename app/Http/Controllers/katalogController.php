<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;

class katalogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transaksi = transaksi::all();
        return view('/owner/katalog', ['transaksi' => $transaksi]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('data.create-katalog');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaksi = transaksi::find($id);
   return view('edit.edit-katalog', ['transaksi' => $transaksi]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka'
        ];
        $this->validate($request,[
                'plat_mobil' => 'required',
                'tipe_mobil' => 'required',
                'tahun_mobil' => 'required|numeric',
                'gbr_mobil',
                'hrg_pembelian' => 'required|numeric',
                'hrg_penjualan' => 'required|numeric',
                'ket_mobil' => 'required'
        ], $message);

     
        $transaksi = transaksi::find($id);
        $transaksi->plat_mobil = $request->plat_mobil;
        $transaksi->tipe_mobil = $request->tipe_mobil;
        $transaksi->tahun_mobil = $request->tahun_mobil;
        $transaksi->hrg_pembelian = $request->hrg_pembelian;
        $transaksi->hrg_penjualan = $request->hrg_penjualan;
        if ($request->file('gbr_mobil') == "") {
            $transaksi->gbr_mobil= $transaksi->gbr_mobil;
        }
        else {
            $file = $request->file('gbr_mobil');
            $nama_file = time() . "_" . $file->getClientOriginalName();
            $tujuan_upload = 'data_file';
            $file->move($tujuan_upload, $nama_file);
            $transaksi->gbr_mobil = $nama_file;
        }
        $transaksi->ket_mobil = $request->ket_mobil;
        $transaksi->save();
        return redirect('/katalog')->with('sukses', 'Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaksi = transaksi::find($id);
    	$transaksi->delete();
 
    	return redirect('/katalog');
    }

    // menampilkan data transaksi yg terjual
public function trash()
{
    	// mengambil data transaksi yg terjual
    	$transaksi = transaksi::onlyTrashed()->get();
    	return view('/owner/terjual', ['transaksi' => $transaksi]);
}
}
