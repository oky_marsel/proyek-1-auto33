<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengguna;
use App\User;
class PenggunaController extends Controller
{
    
    public function index()
    {
        $pengguna = pengguna::all();
        return view('/owner/user', ['pengguna' => $pengguna]);
    }


    public function create()
    {
        return view('data.create-user');
    }

    
    public function store(Request $request)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka'
        ];

        $this->validate($request, [
            'nik' => 'required|numeric',
            'nama' => 'required',
            'username' => 'required',
            'email' => 'required',
            'tmp_lhr' => 'required',
            'lahir_usr' => 'required|date',
            'jk_usr' => 'required',
            'hp_usr' => 'required|numeric',
            'alamat' => 'required',
            'role' => 'required'
        ], $message);

        // insert user
        $user = new \App\User;
        $user->role = $request->role;
        $user->name = $request->username;
        $user->email = $request->email;
        $user->password = bcrypt('12345678');
        $user->remember_token = str_random(60);
        $user->save();

        // insert tabel pengguna
        // $request->request->add(['user_id' => $user->id]);
        $pengguna = pengguna::create([
            'user_id' => $user->id,
            'nik' => $request->nik,
            'nama' => $request->nama,
            'username' => $request->username,
            'email' => $request->email,
            'tmp_lhr' => $request->tmp_lhr,
            'lahir_usr' => $request->lahir_usr,
            'jk_usr' => $request->jk_usr,
            'hp_usr' => $request->hp_usr,
            'alamat' => $request->alamat,
            'role' => $request->role
        ]);
    

        return redirect('/user')->with('sukses', 'Data Berhasil Ditambah');
    }


    public function edit($id)
    {
        $pengguna = pengguna::find($id);
        return view('edit.edit-user', ['pengguna' => $pengguna]);
    }


public function update($id, Request $request)
{
    $message = [
        'required' => ':attribute wajib diisi!!!',
        'min' => ':attribute harus diisi minimal :min karakter!!!',
        'max' => ':attribute harus diisi maksimal :max karakter!!!',
        'date' => ':attribute harus diisi tanggal',
        'numeric' => ':attribute harus diisi angka'
    ];
    $this->validate($request,[
            'nama' => 'required',
            'username' => 'required',
            'email' => 'required',
            'tmp_lhr' => 'required',
            'lahir_usr' => 'required|date',
            'jk_usr' => 'required',
            'hp_usr' => 'required|numeric',
            'alamat' => 'required',
            'role' => 'required'
    ], $message);
 
    $pengguna = pengguna::find($id);
    $pengguna->nama = $request->nama;
    $pengguna->username = $request->username;
    $pengguna->email = $request->email;
    $pengguna->tmp_lhr = $request->tmp_lhr;
    $pengguna->lahir_usr = $request->lahir_usr;
    $pengguna->jk_usr = $request->jk_usr;
    $pengguna->hp_usr = $request->hp_usr;
    $pengguna->alamat = $request->alamat;
    $pengguna->role = $request->role;
    $pengguna->save();
    return redirect('/user')->with('sukses', 'Data Berhasil Diubah');
}


public function destroy($id)
{
	// hapus file
	// $gambar = pengguna::where('id',$id)->first();
	// File::delete('data_file/'.$gambar->file);
    $pengguna = pengguna::find($id);
	// hapus data
    // pengguna::where('id',$id)->delete();
    $pengguna->delete();
 
	return redirect()->back()->with('sukses', 'Data Berhasil Dihapus');
}
}
