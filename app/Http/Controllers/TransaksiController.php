<?php

namespace App\Http\Controllers;

use App\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransaksiController extends Controller
{
    
    public function index()
    {
        $transaksi = transaksi::all();
        return view('/owner/transaksi', ['transaksi' => $transaksi]);
        
    }
    public function create()
    {
        return view('data.create-trx');
    }

    public function store(Request $request)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka',
            'image' => ':attribute harus file .jpeg, .png, .jpg',
            'mimes' => ':attribute harus berformat :mimes'
        ];

        $this->validate($request, [
            'id_trx' => 'required',
            'mesin_mobil' => 'required',
            'plat_mobil' => 'required',
            'tipe_mobil' => 'required',
            'tahun_mobil' => 'required|min:4|max:4',
            'gbr_mobil' => 'required',
            'id_penjual' => 'required',
            'tgl_pembelian' => 'required|date',
            'hrg_pembelian' => 'required|numeric',
            'ket_mobil' => 'required'
        ], $message);

        // menyimpan data file yang diupload ke variabel $file
        $file = $request->file('gbr_mobil');

        $nama_file = time() . "_" . $file->getClientOriginalName();

        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'data_file';
        $file->move($tujuan_upload, $nama_file);


        transaksi::create([
            'id_trx' => $request->id_trx,
            'mesin_mobil' => $request->mesin_mobil,
            'plat_mobil' => $request->plat_mobil,
            'tipe_mobil' => $request->tipe_mobil,
            'tahun_mobil' => $request->tahun_mobil,
            'gbr_mobil' => $nama_file,
            'ket_mobil' => $request->ket_mobil,
            'id_penjual' => $request->id_penjual,
            'hrg_pembelian' => $request->hrg_pembelian,
            'tgl_pembelian' => $request->tgl_pembelian
        ]);

        return redirect('/transaksi')->with('sukses', 'Data Berhasil Ditambah');
    }

    public function edit($id)
    {
        $transaksi = transaksi::find($id);
   return view('edit.edit-transaksi', ['transaksi' => $transaksi]);
    }

    public function update(Request $request, $id)
    {
        $message = [
            'required' => ':attribute wajib diisi!!!',
            'min' => ':attribute harus diisi minimal :min karakter!!!',
            'max' => ':attribute harus diisi maksimal :max karakter!!!',
            'date' => ':attribute harus diisi tanggal',
            'numeric' => ':attribute harus diisi angka'
        ];
        $this->validate($request,[
                'id_pembeli' => 'required',
                'tgl_penjualan' => 'required|date'
        ], $message);

     
        $transaksi = transaksi::find($id);
        $transaksi->id_pembeli = $request->id_pembeli;
        $transaksi->tgl_penjualan = $request->tgl_penjualan;
        $transaksi->save();
        return redirect('/transaksi')->with('sukses', 'Data Berhasil Diubah');
    }

    public function destroy($id)
    {

    }
}
