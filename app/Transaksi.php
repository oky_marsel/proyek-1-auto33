<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Transaksi extends Model
{
    use SoftDeletes;
    protected $table = 'transaksi';
    protected $fillable = [
        'id_trx',
        'mesin_mobil',
        'tipe_mobil',
        'plat_mobil',
        'tahun_mobil',
        'ket_mobil',
        'gbr_mobil',
        'id_penjual',
        'tgl_pembelian',
        'hrg_pembelian',
        'id_pembeli',
        'tgl_penjualan',
        'hrg_penjualan'
    ];
    protected $dates = ['deleted_at'];
}
