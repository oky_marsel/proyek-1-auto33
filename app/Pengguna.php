<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pengguna extends Model
{
    use SoftDeletes;
    protected $table = 'pengguna';
    protected $fillable = [
        'nik',
        'user_id',
        'nama',
        'username',
        'email',
        'tmp_lhr',
        'lahir_usr',
        'jk_usr',
        'hp_usr',
        'alamat',
        'role'
    ];
    protected $dates = ['deleted_at'];
}
