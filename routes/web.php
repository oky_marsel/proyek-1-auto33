<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Route::get('/index', function () {
//     return view('/pengunjung/home');
// });

Route::get('/dashboard', function () {
    return view('/owner/dashboard');
});

Route::get('/about', function () {
    return view('about');
});

Route::group(['middleware' => ['auth','CheckLevel:owner']],function(){
Route::get('/user', 'PenggunaController@index');
Route::get('/createuser', 'PenggunaController@create');
Route::post('/storeuser', 'PenggunaController@store');
Route::get('/desuser:{id}', 'PenggunaController@destroy');
Route::get('/edituser/{id}', 'PenggunaController@edit');
Route::put('/upduser/{id}', 'PenggunaController@update');

Route::get('/katalog', 'katalogController@index');
Route::get('/createktlg', 'katalogController@create');
Route::post('/storektlg', 'katalogController@store');
Route::get('/desktlg:{id}', 'katalogController@destroy');
Route::get('/editktlg/{id}', 'katalogController@edit');
Route::put('/updktlg/{id}', 'katalogController@update');

Route::get('/transaksi', 'TransaksiController@index');
Route::get('/createtrx', 'TransaksiController@create');
Route::post('/storetrx', 'TransaksiController@store');
Route::get('/destrx:{id}', 'TransaksiController@destroy');
Route::get('/editrx/{id}', 'TransaksiController@edit');
Route::put('/updtrx/{id}', 'TransaksiController@update');
Route::get('/terjual', 'KatalogController@trash');
});

Route::group(['middleware' => ['auth','CheckLevel:pegawai,owner']],function(){
    Route::get('/katalog', 'katalogController@index');
    Route::get('/createktlg', 'katalogController@create');
    Route::post('/storektlg', 'katalogController@store');
    Route::get('/desktlg:{id}', 'katalogController@destroy');
    Route::get('/editktlg/{id}', 'katalogController@edit');
    Route::put('/updktlg/{id}', 'katalogController@update');
    
    Route::get('/transaksi', 'TransaksiController@index');
    Route::get('/createtrx', 'TransaksiController@create');
    Route::post('/storetrx', 'TransaksiController@store');
    Route::get('/destrx:{id}', 'TransaksiController@destroy');
    Route::get('/editrx/{id}', 'TransaksiController@edit');
    Route::put('/updtrx/{id}', 'TransaksiController@update');
    Route::get('/terjual', 'KatalogController@trash');
});
Auth::routes();

// verifikasi email user
//Auth::routes(['verify' => true]);

Route::get('/', 'HomeController@index');
Route::get('/detil/{id}', 'HomeController@show');
