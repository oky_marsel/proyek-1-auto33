<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pengguna extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengguna', function (Blueprint $table) {
            $table->increments('id');
            $table->increments('id_user');
            $table->string('nik')->unique();
            $table->string('nama');
            $table->string('username');
            $table->string('email');
            $table->string('tmp_lhr');
            $table->date('lahir_usr');
            $table->string('jk_usr');
            $table->string('hp_usr');
            $table->longText('alamat');
            $table->string('role');
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengguna');
    }
}
