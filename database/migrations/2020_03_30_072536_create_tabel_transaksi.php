<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('id_trx')->unique();
            $table->string('mesin_mobil')->unique();
            $table->string('plat_mobil')->unique();
            $table->string('tipe_mobil');
            $table->year('tahun_mobil');
            $table->string('ket_mobil');
            $table->string('gbr_mobil');
            $table->string('id_penjual');
            $table->date('tgl_pembelian');
            $table->integer('hrg_pembelian');
            $table->string('id_pembeli')->nullable();
            $table->integer('hrg_penjualan')->nullable();
            $table->date('tgl_penjualan')->nullable();
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
